package com.example.movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.movie.models.MovieCast;
import com.example.movie.models.MovieCastId;

@Repository
public interface MovieCastRepository extends JpaRepository<MovieCast, MovieCastId> {

}

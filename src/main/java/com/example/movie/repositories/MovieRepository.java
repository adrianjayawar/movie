package com.example.movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.movie.models.Movie;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {

}

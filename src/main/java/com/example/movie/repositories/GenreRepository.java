package com.example.movie.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.movie.models.Genre;

@Repository
public interface GenreRepository extends JpaRepository<Genre, Long> {

}

package com.example.movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movie.modelDTO.MovieCastDTO;
import com.example.movie.models.MovieCast;
import com.example.movie.repositories.MovieCastRepository;

@RestController
@RequestMapping("/api")
public class MovieCastController {
	@Autowired
	MovieCastRepository movieCastRepository;
	
	@GetMapping("/moviecasts/read")
	public HashMap<String, Object> getAllMovieCastDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		ArrayList<MovieCastDTO> listMovieCastDTO = new ArrayList<MovieCastDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(MovieCast movieCast : listMovieCastEntity) {
			MovieCastDTO movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
			
			listMovieCastDTO.add(movieCastDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Movie Cast's Data Success");
		result.put("Data", listMovieCastDTO);
		
		return result;
	}
	
	@PostMapping("/moviecasts/create")
	public HashMap<String, Object> createMovieCastDTO(@Valid @RequestBody MovieCastDTO movieCastDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		MovieCast movieCastEntity = modelMapper.map(movieCastDTO, MovieCast.class);;
		
		movieCastRepository.save(movieCastEntity);
		
		movieCastDTO.setId(movieCastEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie Cast Success");
		result.put("Data", movieCastDTO);
		
		return result;
	}
	
	@PutMapping("/moviecasts/update/{actId}/{movId}")
	public HashMap<String, Object> updateMovieCastDTO(@PathVariable(value = "actId") long actId, @PathVariable(value = "movId") long movId, @Valid @RequestBody MovieCastDTO movieCastDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		boolean found = false;
		
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(MovieCast movieCast : listMovieCastEntity) {
			if((movieCast.getId().getActId() == actId) && (movieCast.getId().getMovId() == movId)) {
				movieCast.setRole(movieCastDetails.getRole());
				
				movieCastRepository.save(movieCast);
			    
				movieCastDetails = modelMapper.map(movieCast, MovieCastDTO.class);
				
				found = true;
			}
		}			
	    
		if(found) {
			result.put("Status", 200);
			result.put("Message", "Update Movie Cast Success");
			result.put("Data", movieCastDetails);
		}
		else {
			result.put("Status", 400);
			result.put("Message", "Update Movie Cast Failed");
		}
		
		return result;
	}
	
	@DeleteMapping("/moviecasts/delete/{actId}/{movId}")
	public HashMap<String, Object> deleteMovieCastDTO(@PathVariable(value = "actId") long actId, @PathVariable(value = "movId") long movId) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		boolean found = false;
		
		ArrayList<MovieCast> listMovieCastEntity = (ArrayList<MovieCast>) movieCastRepository.findAll();
		
		ModelMapper modelMapper = new ModelMapper();
		MovieCastDTO movieCastDTO = new MovieCastDTO();
		
		for(MovieCast movieCast : listMovieCastEntity) {
			movieCastDTO = modelMapper.map(movieCast, MovieCastDTO.class);
			
			if((movieCast.getId().getActId() == actId) && (movieCast.getId().getMovId() == movId)) {				
				movieCastRepository.delete(movieCast);
				
				found = true;
			}
		}
		
		if(found) {
			result.put("Status", 200);
			result.put("Message", "Delete Movie Cast Success");
			result.put("Data", movieCastDTO);
		}
		else {
			result.put("Status", 400);
			result.put("Message", "Delete Movie Cast Failed");
		}
		
		return result;
	}
}

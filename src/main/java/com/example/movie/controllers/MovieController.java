package com.example.movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movie.exceptions.ResourceNotFoundException;
import com.example.movie.modelDTO.MovieDTO;
import com.example.movie.models.Movie;
import com.example.movie.repositories.MovieRepository;

@RestController
@RequestMapping("/api")
public class MovieController {
	@Autowired
	MovieRepository movieRepository;
	
	@GetMapping("/movies/read")
	public HashMap<String, Object> getAllMovieDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Movie> listMovieEntity = (ArrayList<Movie>) movieRepository.findAll();
		ArrayList<MovieDTO> listMovieDTO = new ArrayList<MovieDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Movie movie : listMovieEntity) {
			MovieDTO movieDTO = modelMapper.map(movie, MovieDTO.class);
			
			listMovieDTO.add(movieDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Movie's Data Success");
		result.put("Data", listMovieDTO);
		
		return result;
	}
	
	@PostMapping("/movies/create")
	public HashMap<String, Object> createMovieDTO(@Valid @RequestBody MovieDTO movieDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Movie movieEntity = modelMapper.map(movieDTO, Movie.class);;
		
		movieRepository.save(movieEntity);
		
		movieDTO.setId(movieEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
	
	@PutMapping("/movies/update/{id}")
	public HashMap<String, Object> updateMovieDTO(@PathVariable(value = "id") Long id, @Valid @RequestBody MovieDTO movieDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Movie movieEntity = movieRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Movie", "id", id));
		movieEntity = modelMapper.map(movieDetails, Movie.class);
		
		movieEntity.setId(id);

		movieRepository.save(movieEntity);
		
		movieDetails = modelMapper.map(movieEntity, MovieDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Movie Success");
		result.put("Data", movieDetails);
		
		return result;
	}
	
	@DeleteMapping("/movies/delete/{id}")
	public HashMap<String, Object> deleteMovieDTO(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Movie movieEntity = movieRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Movie", "id", id));
		MovieDTO movieDTO = modelMapper.map(movieEntity, MovieDTO.class);
		
		movieRepository.delete(movieEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Movie Success");
		result.put("Data", movieDTO);
		
		return result;
	}
}

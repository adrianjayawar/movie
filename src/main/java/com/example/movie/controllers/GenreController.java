package com.example.movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movie.exceptions.ResourceNotFoundException;
import com.example.movie.modelDTO.GenreDTO;
import com.example.movie.models.Genre;
import com.example.movie.repositories.GenreRepository;

@RestController
@RequestMapping("/api")
public class GenreController {
	@Autowired
	GenreRepository genreRepository;
	
	@GetMapping("/genres/read")
	public HashMap<String, Object> getAllGenreDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Genre> listGenreEntity = (ArrayList<Genre>) genreRepository.findAll();
		ArrayList<GenreDTO> listGenreDTO = new ArrayList<GenreDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Genre genre : listGenreEntity) {
			GenreDTO genreDTO = modelMapper.map(genre, GenreDTO.class);
			
			listGenreDTO.add(genreDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Genre's Data Success");
		result.put("Data", listGenreDTO);
		
		return result;
	}
	
	@PostMapping("/genres/create")
	public HashMap<String, Object> createGenreDTO(@Valid @RequestBody GenreDTO genreDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Genre genreEntity = modelMapper.map(genreDTO, Genre.class);;
		
		genreRepository.save(genreEntity);
		
		genreDTO.setId(genreEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
	
	@PutMapping("/genres/update/{id}")
	public HashMap<String, Object> updateGenreDTO(@PathVariable(value = "id") Long id, @Valid @RequestBody GenreDTO genreDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Genre genreEntity = genreRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Genre", "id", id));
		genreEntity = modelMapper.map(genreDetails, Genre.class);
		
		genreEntity.setId(id);

		genreRepository.save(genreEntity);
		
		genreDetails = modelMapper.map(genreEntity, GenreDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Genre Success");
		result.put("Data", genreDetails);
		
		return result;
	}
	
	@DeleteMapping("/genres/delete/{id}")
	public HashMap<String, Object> deleteGenreDTO(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Genre genreEntity = genreRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Genre", "id", id));
		GenreDTO genreDTO = modelMapper.map(genreEntity, GenreDTO.class);
		
		genreRepository.delete(genreEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Genre Success");
		result.put("Data", genreDTO);
		
		return result;
	}
}

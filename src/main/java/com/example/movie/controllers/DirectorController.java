package com.example.movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movie.exceptions.ResourceNotFoundException;
import com.example.movie.modelDTO.DirectorDTO;
import com.example.movie.models.Director;
import com.example.movie.repositories.DirectorRepository;

@RestController
@RequestMapping("/api")
public class DirectorController {
	@Autowired
	DirectorRepository directorRepository;
	
	@GetMapping("/directors/read")
	public HashMap<String, Object> getAllDirectorDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Director> listDirectorEntity = (ArrayList<Director>) directorRepository.findAll();
		ArrayList<DirectorDTO> listDirectorDTO = new ArrayList<DirectorDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Director director : listDirectorEntity) {
			DirectorDTO directorDTO = modelMapper.map(director, DirectorDTO.class);
			
			listDirectorDTO.add(directorDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Director's Data Success");
		result.put("Data", listDirectorDTO);
		
		return result;
	}
	
	@PostMapping("/directors/create")
	public HashMap<String, Object> createDirectorDTO(@Valid @RequestBody DirectorDTO directorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Director directorEntity = modelMapper.map(directorDTO, Director.class);;
		
		directorRepository.save(directorEntity);
		
		directorDTO.setId(directorEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
	
	@PutMapping("/directors/update/{id}")
	public HashMap<String, Object> updateDirectorDTO(@PathVariable(value = "id") Long id, @Valid @RequestBody DirectorDTO directorDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Director directorEntity = directorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Director", "id", id));
		directorEntity = modelMapper.map(directorDetails, Director.class);
		
		directorEntity.setId(id);

		directorRepository.save(directorEntity);
		
		directorDetails = modelMapper.map(directorEntity, DirectorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Director Success");
		result.put("Data", directorDetails);
		
		return result;
	}
	
	@DeleteMapping("/directors/delete/{id}")
	public HashMap<String, Object> deleteDirectorDTO(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Director directorEntity = directorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Director", "id", id));
		DirectorDTO directorDTO = modelMapper.map(directorEntity, DirectorDTO.class);
		
		directorRepository.delete(directorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Director Success");
		result.put("Data", directorDTO);
		
		return result;
	}
}

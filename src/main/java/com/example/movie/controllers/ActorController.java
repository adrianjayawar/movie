package com.example.movie.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.movie.exceptions.ResourceNotFoundException;
import com.example.movie.modelDTO.ActorDTO;
import com.example.movie.models.Actor;
import com.example.movie.repositories.ActorRepository;

@RestController
@RequestMapping("/api")
public class ActorController {
	@Autowired
	ActorRepository actorRepository;
	
	@GetMapping("/actors/read")
	public HashMap<String, Object> getAllActorDTO() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ArrayList<Actor> listActorEntity = (ArrayList<Actor>) actorRepository.findAll();
		ArrayList<ActorDTO> listActorDTO = new ArrayList<ActorDTO>();
		
		ModelMapper modelMapper = new ModelMapper();
		
		for(Actor actor : listActorEntity) {
			ActorDTO actorDTO = modelMapper.map(actor, ActorDTO.class);
			
			listActorDTO.add(actorDTO);
		}
		
		result.put("Status", 200);
		result.put("Message", "Read All Actor's Data Success");
		result.put("Data", listActorDTO);
		
		return result;
	}
	
	@PostMapping("/actors/create")
	public HashMap<String, Object> createActorDTO(@Valid @RequestBody ActorDTO actorDTO){
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Actor actorEntity = modelMapper.map(actorDTO, Actor.class);;
		
		actorRepository.save(actorEntity);
		
		actorDTO.setId(actorEntity.getId());
		
		result.put("Status", 200);
		result.put("Message", "Create New Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
		
	@PutMapping("/actors/update/{id}")
	public HashMap<String, Object> updateActorDTO(@PathVariable(value = "id") Long id, @Valid @RequestBody ActorDTO actorDetails) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Actor actorEntity = actorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Actor", "id", id));
		actorEntity = modelMapper.map(actorDetails, Actor.class);
		
		actorEntity.setId(id);

		actorRepository.save(actorEntity);
		
		actorDetails = modelMapper.map(actorEntity, ActorDTO.class);
		
		result.put("Status", 200);
		result.put("Message", "Update Actor Success");
		result.put("Data", actorDetails);
		
		return result;
	}
	
	@DeleteMapping("/actors/delete/{id}")
	public HashMap<String, Object> deleteActorDTO(@PathVariable(value = "id") Long id) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Actor actorEntity = actorRepository.findById(id).orElseThrow(() -> new ResourceNotFoundException("Actor", "id", id));
		ActorDTO actorDTO = modelMapper.map(actorEntity, ActorDTO.class);
		
		actorRepository.delete(actorEntity);
		
		result.put("Status", 200);
		result.put("Message", "Delete Actor Success");
		result.put("Data", actorDTO);
		
		return result;
	}
}
